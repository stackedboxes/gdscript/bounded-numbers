#
# Bounded Numbers
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2020 Leandro Motta Barros
#

extends Node

static func new(initialValue: float = 0.0):
	return BNumber.new(initialValue)


# A Bounded Number.
#
# This design using an inner class is not really what I intended, but that was
# the workaround I found around some issues with `class_name` in GDScript as of
# May 2020. (We cannot use `myClassName.new()` from a method of the class
# itself). This implementation is a bit odd, but at least it allows clients to
# use the syntax I desired.
class BNumber extends Reference:
	# The Bounded Number value. Will always be between -1.0 and 1.0, inclusive.
	# Assigning values outside of this range will get them clamped.
	var value: float = 0.0 setget _setValue


	# Constructs a BNumber, initializing it to the given value. This initial value
	# will be clamped to be between the valid interval [-1.0, +1.0].
	func _init(initialValue: float = 0.0):
		_setValue(initialValue)


	# Adds other number to this BNumber and returns the result. The `other`
	# parameter can be a BNumber or, for convenience, a built-in GDScript number
	# (either a float or integer, which will be converted to a BNumber to ensure we
	# stay in the valid [-1, +1] range).
	func plus(other) -> BNumber:
		var bNumberClass = self.get_script()
		var amount = _valueFromAny(other)

		var d := 0.0
		if amount > 0:
			d = 1.0 - value
		elif amount < 0:
			d = value + 1.0

		return bNumberClass.new(self.value + d * amount)


	# Subtracts other number from this BNumber and returns the result. The `other`
	# parameter can be a BNumber or, for convenience, a built-in GDScript number
	# (either a float or integer, which will be converted to a BNumber to ensure we
	# stay in the valid [-1, +1] range).
	func minus(other) -> BNumber:
		var v = _valueFromAny(other)
		return self.plus(-v)


	# Sets the BNumber value, clamping to the valid range. This is private. Client
	# code shall just set the value attribute as desired.
	func _setValue(newValue: float):
		value = clamp(newValue, -1.0, 1.0)


	# Extracts the value (as a float) from "any" type. "Any" is really limited to
	# floats, integers and BNumbers.
	static func _valueFromAny(x) -> float:
		if typeof(x) == TYPE_REAL || typeof(x) == TYPE_INT:
			return float(x)

		return x.value


# The blend operator. Blends from `x` to `y` using `w` as the weight.
static func blend(x, y, w) -> BNumber:
	var xx = BNumber._valueFromAny(x)
	var yy = BNumber._valueFromAny(y)
	var ww = BNumber._valueFromAny(w)

	ww = 1 - ((1 - ww) / 2)
	return BNumber.new(yy * ww + xx * (1 - ww))
