#
# Bounded Numbers
# StackedBoxes' GDScript Hodgepodge
#
# Copyright (c) 2020 Leandro Motta Barros
#

extends SceneTree

var BNumber
var Assert

func _init():
	BNumber = preload("res://BNumber.gd")
	Assert = preload("res://Assert.gd")

	TestConstruction()
	TestSetter()

	TestAdditionBNumber()
	TestAdditionFloat()
	TestAdditionInt()
	TestSubtractionBNumber()
	TestSubtractionFloat()
	TestSubtractionInt()

	TestBlendBNumber()
	TestBlendFloat()
	TestBlendMixed()

	quit()


# Tests BNumber construction.
func TestConstruction() -> void:
	var bn = BNumber.new()
	Assert.isEqual(0.0, bn.value)

	bn = BNumber.new(0.0)
	Assert.isEqual(0.0, bn.value)

	bn = BNumber.new(-0.0)
	Assert.isEqual(0.0, bn.value)

	bn = BNumber.new(1.0)
	Assert.isEqual(1.0, bn.value)

	bn = BNumber.new(-1.0)
	Assert.isEqual(-1.0, bn.value)

	bn = BNumber.new(0.123)
	Assert.isClose(0.123, bn.value, 1e-6)

	bn = BNumber.new(-0.911)
	Assert.isClose(-0.911, bn.value, 1e-6)

	bn = BNumber.new(1.22)
	Assert.isEqual(1.0, bn.value)

	bn = BNumber.new(-10.0)
	Assert.isEqual(-1.0, bn.value)


# Tests the setter.
func TestSetter() -> void:
	var bn = BNumber.new()
	Assert.isEqual(0.0, bn.value)

	bn.value = 0.444
	Assert.isClose(0.444, bn.value, 1e-6)

	bn.value = -0.888
	Assert.isClose(-0.888, bn.value, 1e-6)

	bn.value = 1.888
	Assert.isEqual(1.0, bn.value)

	bn.value = -1.111
	Assert.isEqual(-1.0, bn.value)


# Tests addition with a BNumber parameter.
func TestAdditionBNumber() -> void:
	var a = BNumber.new(0.0)
	var b = BNumber.new(0.0)
	Assert.isEqual(0.0, a.plus(b).value)
	Assert.isEqual(0.0, b.plus(a).value)

	a = BNumber.new(1.0)
	b = BNumber.new(0.0)
	Assert.isClose(1.0, a.plus(b).value, 1e-6)
	Assert.isClose(1.0, b.plus(a).value, 1e-6)

	a = BNumber.new(1.0)
	b = BNumber.new(-1.0)
	Assert.isClose(-1.0, a.plus(b).value, 1e-6)
	Assert.isClose(1.0, b.plus(a).value, 1e-6)

	a = BNumber.new(1.0)
	b = BNumber.new(0.5)
	Assert.isClose(1.0, a.plus(b).value, 1e-6)
	Assert.isClose(1.0, b.plus(a).value, 1e-6)

	a = BNumber.new(0.5)
	b = BNumber.new(0.5)
	Assert.isClose(0.75, a.plus(b).value, 1e-6)
	Assert.isClose(0.75, b.plus(a).value, 1e-6)

	a = BNumber.new(1.0)
	b = BNumber.new(-0.5)
	Assert.isEqual(0.0, a.plus(b).value)
	Assert.isEqual(1.0, b.plus(a).value)

	a = BNumber.new(1.0)
	b = BNumber.new(0.2)
	Assert.isClose(1.0, a.plus(b).value, 1e-6)
	Assert.isClose(1.0, b.plus(a).value, 1e-6)

	a = BNumber.new(-1.0)
	b = BNumber.new(0.2)
	Assert.isClose(-0.6, a.plus(b).value, 1e-6)
	Assert.isClose(-1.0, b.plus(a).value, 1e-6)

	a = BNumber.new(-1.0)
	b = BNumber.new(-0.2)
	Assert.isClose(-1.0, a.plus(b).value, 1e-6)
	Assert.isClose(-1.0, b.plus(a).value, 1e-6)

	a = BNumber.new(0.3)
	b = BNumber.new(0.2)
	Assert.isClose(0.44, a.plus(b).value, 1e-6)
	Assert.isClose(0.44, b.plus(a).value, 1e-6)

	a = BNumber.new(0.8)
	b = BNumber.new(0.2)
	Assert.isClose(0.84, a.plus(b).value, 1e-6)
	Assert.isClose(0.84, b.plus(a).value, 1e-6)

	a = BNumber.new(0.7)
	b = BNumber.new(-0.3)
	Assert.isClose(0.19, a.plus(b).value, 1e-6)
	Assert.isClose(0.61, b.plus(a).value, 1e-6)

	a = BNumber.new(0.5)
	b = BNumber.new(-0.2)
	Assert.isClose(0.2, a.plus(b).value, 1e-6)
	Assert.isClose(0.4, b.plus(a).value, 1e-6)

	a = BNumber.new(-0.9)
	b = BNumber.new(-0.4)
	Assert.isClose(-0.94, a.plus(b).value, 1e-6)
	Assert.isClose(-0.94, b.plus(a).value, 1e-6)


# Tests addition with a float parameter.
func TestAdditionFloat() -> void:
	var a = BNumber.new(0.0)
	Assert.isEqual(0.0, a.plus(0.0).value)

	a = BNumber.new(1.0)
	Assert.isClose(1.0, a.plus(0.0).value, 1e-6)

	a = BNumber.new(1.0)
	Assert.isClose(-1.0, a.plus(-1.0).value, 1e-6)

	a = BNumber.new(1.0)
	Assert.isClose(1.0, a.plus(0.5).value, 1e-6)

	a = BNumber.new(0.5)
	Assert.isClose(0.75, a.plus(0.5).value, 1e-6)

	a = BNumber.new(1.0)
	Assert.isEqual(0.0, a.plus(-0.5).value)

	a = BNumber.new(1.0)
	Assert.isClose(1.0, a.plus(0.2).value, 1e-6)

	a = BNumber.new(-1.0)
	Assert.isClose(-0.6, a.plus(0.2).value, 1e-6)

	a = BNumber.new(-1.0)
	Assert.isClose(-1.0, a.plus(-0.2).value, 1e-6)

	a = BNumber.new(0.3)
	Assert.isClose(0.44, a.plus(0.2).value, 1e-6)

	a = BNumber.new(0.8)
	Assert.isClose(0.84, a.plus(0.2).value, 1e-6)

	a = BNumber.new(0.7)
	Assert.isClose(0.19, a.plus(-0.3).value, 1e-6)

	a = BNumber.new(0.5)
	Assert.isClose(0.2, a.plus(-0.2).value, 1e-6)

	a = BNumber.new(-0.9)
	Assert.isClose(-0.94, a.plus(-0.4).value, 1e-6)


# Tests addition with an integer parameter.
func TestAdditionInt() -> void:
	var a = BNumber.new(0.0)
	Assert.isEqual(0.0, a.plus(0).value)

	a = BNumber.new(1.0)
	Assert.isClose(1.0, a.plus(0).value, 1e-6)

	a = BNumber.new(1.0)
	Assert.isClose(-1.0, a.plus(-1).value, 1e-6)

	a = BNumber.new(0.33)
	Assert.isClose(1.0, a.plus(1).value, 1e-6)


# Tests subtraction with a BNumber parameter.
func TestSubtractionBNumber() -> void:
	var a = BNumber.new(0.0)
	var b = BNumber.new(0.0)
	Assert.isEqual(0.0, a.minus(b).value)
	Assert.isEqual(0.0, b.minus(a).value)

	a = BNumber.new(1.0)
	b = BNumber.new(0.0)
	Assert.isClose(1.0, a.minus(b).value, 1e-6)
	Assert.isClose(-1.0, b.minus(a).value, 1e-6)

	a = BNumber.new(0.4)
	b = BNumber.new(0.1)
	Assert.isClose(0.26, a.minus(b).value, 1e-6)
	Assert.isClose(-0.34, b.minus(a).value, 1e-6)

	a = BNumber.new(0.7)
	b = BNumber.new(-0.1)
	Assert.isClose(0.73, a.minus(b).value, 1e-6)
	Assert.isClose(-0.73, b.minus(a).value, 1e-6)

	a = BNumber.new(-0.5)
	b = BNumber.new(-0.6)
	Assert.isClose(0.4, a.minus(b).value, 1e-6)
	Assert.isClose(0.2, b.minus(a).value, 1e-6)


# Tests subtraction with a float parameter.
func TestSubtractionFloat() -> void:
	var a = BNumber.new(0.8)
	Assert.isClose(0.26, a.minus(0.3).value, 1e-6)

	a = BNumber.new(-0.7)
	Assert.isClose(-0.79, a.minus(0.3).value, 1e-6)

	a = BNumber.new(-0.1)
	Assert.isClose(0.34, a.minus(-0.4).value, 1e-6)


# Tests subtraction with an integer parameter.
func TestSubtractionInt() -> void:
	var a = BNumber.new(0.345)
	Assert.isClose(0.345, a.minus(0).value, 1e-6)

	a = BNumber.new(-0.7)
	Assert.isClose(-1.0, a.minus(1).value, 1e-6)

	a = BNumber.new(0.11)
	Assert.isClose(1.0, a.minus(-1).value, 1e-6)


# Tests the blend operator for BNumber parameters.
func TestBlendBNumber() -> void:
	Assert.isSmall(       BNumber.blend(BNumber.new( 0.5), BNumber.new(-0.5), BNumber.new( 0.0)).value, 1e-6)
	Assert.isClose( 0.62, BNumber.blend(BNumber.new( 0.6), BNumber.new( 0.7), BNumber.new(-0.6)).value, 1e-6)
	Assert.isSmall(       BNumber.blend(BNumber.new(-0.3), BNumber.new( 0.7), BNumber.new(-0.4)).value, 1e-6)
	Assert.isClose(-0.18, BNumber.blend(BNumber.new(-0.9), BNumber.new( 0.3), BNumber.new( 0.2)).value, 1e-6)

	Assert.isClose( 0.04, BNumber.blend(BNumber.new( 0.6), BNumber.new(-0.2), BNumber.new( 0.4)).value, 1e-6)
	Assert.isClose( 0.52, BNumber.blend(BNumber.new( 0.6), BNumber.new( 0.4), BNumber.new(-0.2)).value, 1e-6)
	Assert.isClose( 0.28, BNumber.blend(BNumber.new(-0.2), BNumber.new( 0.4), BNumber.new( 0.6)).value, 1e-6)
	Assert.isClose( 0.36, BNumber.blend(BNumber.new(-0.2), BNumber.new( 0.6), BNumber.new( 0.4)).value, 1e-6)
	Assert.isClose( 0.48, BNumber.blend(BNumber.new( 0.4), BNumber.new( 0.6), BNumber.new(-0.2)).value, 1e-6)
	Assert.isClose(-0.08, BNumber.blend(BNumber.new( 0.4), BNumber.new(-0.2), BNumber.new( 0.6)).value, 1e-6)


# Tests the blend operator for float parameters.
func TestBlendFloat() -> void:
	Assert.isSmall(       BNumber.blend( 0.5, -0.5,  0.0).value, 1e-6)
	Assert.isClose( 0.62, BNumber.blend( 0.6,  0.7, -0.6).value, 1e-6)
	Assert.isSmall(       BNumber.blend(-0.3,  0.7, -0.4).value, 1e-6)
	Assert.isClose(-0.18, BNumber.blend(-0.9,  0.3,  0.2).value, 1e-6)

	Assert.isClose( 0.04, BNumber.blend( 0.6, -0.2,  0.4).value, 1e-6)
	Assert.isClose( 0.52, BNumber.blend( 0.6,  0.4, -0.2).value, 1e-6)
	Assert.isClose( 0.28, BNumber.blend(-0.2,  0.4,  0.6).value, 1e-6)
	Assert.isClose( 0.36, BNumber.blend(-0.2,  0.6,  0.4).value, 1e-6)
	Assert.isClose( 0.48, BNumber.blend( 0.4,  0.6, -0.2).value, 1e-6)
	Assert.isClose(-0.08, BNumber.blend( 0.4, -0.2,  0.6).value, 1e-6)


# Tests the blend operator with parameters of mixed types.
func TestBlendMixed() -> void:
	Assert.isSmall(       BNumber.blend(BNumber.new( 0.5),             -0.5 ,              0   ).value, 1e-6)
	Assert.isClose( 0.62, BNumber.blend(             0.6 , BNumber.new( 0.7), BNumber.new(-0.6)).value, 1e-6)
	Assert.isSmall(       BNumber.blend(BNumber.new(-0.3),              0.7 ,             -0.4 ).value, 1e-6)
	Assert.isClose(-0.18, BNumber.blend(            -0.9 ,              0.3 , BNumber.new( 0.2)).value, 1e-6)
	Assert.isClose( 0.04, BNumber.blend(BNumber.new( 0.6),             -0.2,  BNumber.new( 0.4)).value, 1e-6)
	Assert.isClose( 0.52, BNumber.blend(             0.6 , BNumber.new( 0.4),             -0.2 ).value, 1e-6)
	Assert.isClose( 0.28, BNumber.blend(BNumber.new(-0.2), BNumber.new( 0.4),              0.6 ).value, 1e-6)
