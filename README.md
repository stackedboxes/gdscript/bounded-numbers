# Bounded Numbers

*Part of [StackedBoxes' GDScript Hodgepodge](https://gitlab.com/stackedboxes/gdscript/)*

An implementation of Chris Crawford's *Bounded Numbers* (AKA *BNumbers*), said
to be useful for doing calculations with personality models and human
relationships.

See Chris's description of Bounded Numbers
[here](https://www.erasmatazz.com/library/course-description-2018/numbers.html)
and then
[here](https://www.erasmatazz.com/library/course-description-2018/using-bnumber-math.html)
for details. His book *Chris Crawford on Interactive Storytelling, 2nd Ed.* also
has a chapter devoted to them.
